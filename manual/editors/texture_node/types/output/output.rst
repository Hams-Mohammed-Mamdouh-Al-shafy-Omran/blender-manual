.. _bpy.types.TextureNodeOutput:

***********
Output Node
***********

.. figure:: /images/node-types_TextureNodeOutput.webp
   :align: right
   :alt: Output node.

This node receives the result of the node-based texture.


Inputs
======

Color
   The color data that the texture renders.


Properties
==========

Output Name
   The name of the output. (Originally, it was possible for textures to
   have multiple outputs with different names.)


Outputs
=======

This node has no outputs.
