
****
Mask
****

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Mask`
   :Shortcut:  :kbd:`M`

Paint a selection on parts of the mesh to be unaffected by other brushes & tools.
The mask values are shown as a gray-scale overlay.

.. note::

   More information in the
   :doc:`Masking Introduction </sculpt_paint/sculpting/introduction/visibility_masking_face_sets>`.


Brush Settings
==============

General
-------

.. note::

   More info at :ref:`sculpt-tool-settings-brush-settings-general` brush settings
   and on :ref:`sculpt-tool-settings-brush-settings-advanced` brush settings.


Unique
------

.. _bpy.types.Brush.mask_tool:

Mask Tool
   The mask brush has two modes:

   :Draw: Mask drawing.
   :Smooth: Holding :kbd:`Shift` will instead smooth existing masks.
