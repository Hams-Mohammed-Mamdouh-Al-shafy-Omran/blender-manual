
***************
Common Settings
***************

Information on brush settings for every mode can be found in these pages:

Brush
=====

See general and advanced :doc:`Brush </sculpt_paint/brush/brush_settings>` here.

Stroke
======

See the global brush settings for :doc:`Stroke </sculpt_paint/brush/stroke>` settings.


Falloff
=======

See the global brush settings for :doc:`Falloff </sculpt_paint/brush/falloff>` settings.


Cursor
======

See the global brush settings for :doc:`Cursor </sculpt_paint/brush/cursor>` settings.
