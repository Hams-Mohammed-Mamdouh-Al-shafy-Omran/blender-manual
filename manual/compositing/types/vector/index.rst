
################
  Vector Nodes
################

These nodes can be used to manipulate various types of vectors, such as surface normals and speed vectors.

.. toctree::
   :maxdepth: 1

   combine_xyz.rst
   separate_xyz.rst

----------

.. toctree::
   :maxdepth: 1

   normal.rst
   vector_curves.rst
